#include <stdio.h>
#include <stdlib.h>

double *numbers = NULL;
int allocated = 0;
int used = 0;

static int
read_number(void)
{
  double d;
  switch (scanf("%lg ", &d))
  {
  case EOF:
    return EOF;

  case 0:
    fprintf(stderr, "syntax error\n");
    exit(1);
  }

  if (used == allocated)
    {
      if (allocated == 0)
	allocated = 64;
      else
	allocated = 2 * allocated;

      numbers = realloc(numbers, allocated * sizeof(*numbers));
      if (numbers == NULL)
	{
	  perror("realloc");
	  exit(1);
	}
    }
  numbers[used++] = d;
  return 0;
}

static void
read_numbers(void)
{
  while (read_number() != EOF)
    ;
}

static int
comparator(const void *ap, const void *bp)
{
  double a = abs(*(const double *)ap);
  double b = abs(*(const double *)bp);

  return (a > b) - (a < b);
}

int
main(void)
{
  int i;
  int j;

  read_numbers();
  if (used)
    {
      qsort(numbers, used, sizeof(*numbers), &comparator);
      for (i = 0; i < used - 1; i++)
	{
	  numbers[i+1] += numbers[i];
	  for (j = i + 1; j < used - 1; j++)
	    if (abs(numbers[j]) > abs(numbers[j+1]))
	      {
		double tmp = numbers[j];
		numbers[j] = numbers[j+1];
		numbers[j+1] = tmp;
	      }
	    else
	      break;
	}
      printf("%g\n", numbers[used-1]);
    }
}
