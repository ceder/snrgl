#!/bin/sh

rm -f \
    install-sh \
    missing \
    texinfo.tex \
    mkinstalldirs \
    mdate-sh \
    COPYING \
    INSTALL

aclocal
autoheader
automake -a
autoconf
